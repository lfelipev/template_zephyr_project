set(TESTS_FOLDER "${PROJECT_SOURCE_DIR}/tests")
set(SANITY_FOLDER "${TESTS_FOLDER}/sanity")
set(TESTS_REPORT "${SANITY_FOLDER}/tests.report")

add_custom_target(
    unity_tests
    sanitycheck
    -T ${TESTS_FOLDER} 
    --outdir ${SANITY_FOLDER} 
    --detailed-report ${TESTS_REPORT} 
    -N 
    -j4 
    -v
    -i
    --exclude-tag hwtest
)
add_custom_target(
    unity_tests_hw
    sanitycheck 
    -T ${TESTS_FOLDER} 
    --outdir ${SANITY_FOLDER} 
    --detailed-report ${TESTS_REPORT} 
#    -N
    -j4
    -v
    -i
    -t hwtest
    -p nrf52840_pca10056
    --device-testing
    --device-serial /dev/ttyACM0
)

add_custom_target(
    unity_tests_report
    junit2html 
    ${TESTS_REPORT}
)

