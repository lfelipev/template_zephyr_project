################################################################################################
# Do not set CONFIG_UNIT_TEST ON in the code. Run the following commands:                 #
# $ source zephyr.sh
# Go to tests folder and type the command:
# $ sanitycheck --testcase-root . --platform native_posix --outdir ./build/santy-out --detailed-report ./build/tests.report
# The command will run the unit tests and show the report.                                     #
################################################################################################
set(BOARD native_posix)
include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(Project_tests)

set(DUT_SOURCE_DIR "${PROJECT_SOURCE_DIR}/../../..")

list(APPEND HEADERS
    "${DUT_SOURCE_DIR}/include"
    "${DUT_SOURCE_DIR}/build/include/generated"
    "${PROJECT_SOURCE_DIR}/include"
    )

FILE(GLOB SOURCES src/*.c)

list(APPEND SOURCES
    "${DUT_SOURCE_DIR}/src/mylib.c"
    )

add_custom_target(
    unity_test
    ${PROJECT_BINARY_DIR}/zephyr/zephyr.exe
    )

include_directories(${HEADERS})
target_sources(app PRIVATE ${SOURCES})

