/*
 * Copyright (c) 2018 Company 
 *
 */

#include <zephyr.h>
#include <device.h>
#include <misc/printk.h>

#include <ztest.h>

#include "mylib.h"
#include "version.h"

static void test_inc(void)
{
    int count = -1;
    inc(&count);
    zassert_equal(count, 0, NULL);
    inc(&count);
    inc(&count);
    zassert_equal(count, 2, NULL);
    for(int i = 0; i < 10000; i++) {
        inc(&count);
    }
    zassert_equal(count, 10002, NULL);
}


static void test_dec(void)
{
    int count = 100;
    dec(&count);
    zassert_equal(count, 99, NULL);
    dec(&count);
    dec(&count);
    zassert_equal(count, 97, NULL);
    for(int i = 0; i < 90; i++) {
        dec(&count);
    }
    zassert_equal(count, 7, NULL);
}


void test_main(void)
{
    printk("Firmware version: v%d.%d.%d\n", VERSION_MAJOR, VERSION_MINOR, VERSION_BUILD);
    ztest_test_suite(tests_helloworld,
                     ztest_unit_test(test_inc),
                     ztest_unit_test(test_dec)                     
                     );

    ztest_run_test_suite(tests_helloworld);
}
