#ifndef SRC_GLOBAL_PRAMS_H_
#define SRC_GLOBAL_PARAMS_H_

/*
 * Mostra ao usuário o identificador único, de 32-bits, do pager
 * */
int shell_cmd_get_unique_id(int argc, char *argv[]);

/*
 * Mostra ao usuário o nível de bateria do pager
 * */
int shell_cmd_get_battery_lvl(int argc, char *argv[]);

/*
 * Mostra ao usuário o estado atual do pager.
 * Os estados permitidos são:
 * 	- Charging
 * 	- Pairing
 * 	- Sleep
 * 	- Ready
 * */
int shell_cmd_get_pager_state(int argc, char *argv[]);

/*
 * Configura o pager para o estado especificado no 'argv'
 * Os estados permitidos são:
 * 	- Charging
 * 	- Pairing
 * 	- Sleep
 * 	- Ready
 * */
int shell_cmd_set_pager_state(int argc, char *argv[]);

int shell_cmd_params(int argc, char *argv[]);

#endif /* SRC_GLOBAL_PARAMS_H_ */