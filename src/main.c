/*! @file
 *  @brief Basic code.
 *         This is a demonstration of how to use doxygen.
 *
 *  Second explanation.
 */

#include <zephyr.h>
#include "version.h"
#define SYS_LOG_DOMAIN "MAIN"
#include <logging/sys_log.h>
#include "mylib.h"
#include "global_params.h"
#include <shell/shell.h>
#define MY_SHELL_MODULE "sample_module"

struct shell_cmd status_commands[] = {
    {"uid", shell_cmd_get_unique_id, "Shows device uit."},
    {"batttery", shell_cmd_get_battery_lvl, "Shows battery level."},
    {"get_state", shell_cmd_get_pager_state, "Shows pager state."},
    {"set_state", shell_cmd_set_pager_state, "Set pager state."},
    {NULL, NULL, NULL}
};

void main(void)
{
    SHELL_REGISTER(MY_SHELL_MODULE, status_commands);
}
