#include "global_params.h"
#include <zephyr/types.h>
#include <shell/shell.h>
#include <misc/printk.h>

static struct {
    u32_t id;
    u32_t battery_lvl;
    u32_t pager_state;
} __global_params = {
    .pager_state = 0,
    .battery_lvl = 100,
    .id = 0x0ABCDEF0,
};

int get_id() {
    return __global_params.id;
}

int get_battery_lvl() {
    return __global_params.battery_lvl;
}

void set_pager_state() {
    __global_params.pager_state = 1;
}

int get_pager_state() {
    return __global_params.pager_state;
}

int shell_cmd_get_unique_id(int argc, char *argv[]) {
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    printk("id: %X \n", get_id());

    return 0;
}

int shell_cmd_get_battery_lvl(int argc, char *argv[]) {
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    printk("battery lvl: %X \n", get_battery_lvl());

    return 0;
}

int shell_cmd_get_pager_state(int argc, char *argv[]) {
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    printk("pager state: %X \n", get_pager_state());

    return 0;
}

int shell_cmd_set_pager_state(int argc, char *argv[]) {
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    printk("pager state set to 1.\n");

    return 0;
}
